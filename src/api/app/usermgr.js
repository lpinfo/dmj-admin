import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-app/usermgr/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-app/usermgr/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-app/usermgr/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/blade-app/usermgr/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  // alert(row.qpccode);
  let Base64 = require('js-base64').Base64;
  row.qpccode = Base64.encode(row.qpccode);
  return request({
    url: '/api/blade-app/usermgr/submit',
    method: 'post',
    data: row
  })
}

