import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-app/usercheck/page',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-app/usercheck/detail',
    method: 'get',
    params: {
      id
    }
  })
}


export const remove = (ids) => {
  return request({
    url: '/api/blade-app/usercheck/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-app/usercheck/submit',
    method: 'post',
    data: row
  })
}
////////////////////

export const acceptUserInfo = (ids) => {
  return request({
    url: '/api/blade-app/usercheck/acceptUserInfo',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const refuseUserInfo = (row) => {
  return request({
    url: '/api/blade-app/usercheck/refuseUserInfo',
    method: 'post',
    data: row
  })
}
