import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-app/article/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-app/article/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-app/article/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  row.appflag="1";
  return request({
    url: '/api/blade-app/article/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-app/article/submit',
    method: 'post',
    data: row
  })
}

