import request from '@/router/axios';

export const getPage = (current, size, params) => {
  return request({
    url: '/api/blade-system/tenant/page',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-system/tenant/detail',
    method: 'get',
    params: {
      id
    }
  })
}

