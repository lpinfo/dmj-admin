export const optionParent = {
  height: 'auto',
  tip: false,
  // searchShow: true,
  // searchMenuSpan: 10,
  border: true,
  addBtn:false,
  delBtn:false,
  index: false,
  selection: false,
  viewBtn: true,
  dialogWidth: 880,
  dialogClickModal: false,
  column: [
    {
      hide:true,
      label: "编号",
      prop: "code",
      addDisabled: true,
      editDisabled: true,
      editDisplay:false,
      addDisplay:false,
    },
    {
      label: "领域名称",
      prop: "dictValue",
      align: "center",
    },

  ]
};

export const optionChild = {
  height: 'auto',
  calcHeight: 95,
  addBtn:false,
  tip: false,
  searchShow: true,
  searchMenuSpan: 10,
  tree: true,
  border: true,
  index: false,
  selection: false,
  viewBtn: true,
  delBtn:false,
  dialogWidth: 880,
  dialogClickModal: false,
  column: [
    {
      hide:true,
      label: "字典编号",
      prop: "code",
      addDisabled: true,
      editDisabled: true,
      editDisplay:false,
      addDisplay:false,
    },
    {
      label: "字典名称",
      prop: "dictValue",
      align: "center",
    },
    {
      label: "上级字典",
      prop: "parentId",
      type: "tree",

      dicData: [],
      hide: true,
      props: {
        label: "title"
      },
      addDisabled: true,
      addDisplay:false,
      editDisabled: true,
      editDisplay:false,
    },
    {
      hide:true,
      label: "字典键值",
      prop: "dictKey",
      addDisabled: true,
      addDisplay:false,
      editDisabled: true,
      editDisplay:false,
    },


  ]
};
