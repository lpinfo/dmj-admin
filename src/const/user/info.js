export default {
  column: [{
    label: '个人信息',
    prop: 'info',
    option: {
      submitText: '修改',
      column: [{
        label: '头像',
        type: 'upload',
        listType: 'picture-img',
        propsHttp: {
          res: 'data',
          url: 'link',
        },
        canvasOption: {
          text: ' ',
          ratio: 0.1
        },
        action: '/api/blade-app/filemgr/upload',
        tip: '只能上传jpg/png用户头像，且不超过500kb',
        span: 12,
        row: true,
        prop: 'avatar'
      }, {
        label: '姓名',
        span: 12,
        row: true,
        prop: 'name'
      }, {
        label: '手机号',
        span: 12,
        row: true,
        prop: 'phone'
      }]
    }
  }]
}
